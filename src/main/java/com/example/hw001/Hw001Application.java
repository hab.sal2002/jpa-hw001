package com.example.hw001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hw001Application {

    public static void main(String[] args) {
        SpringApplication.run(Hw001Application.class, args);
    }

}
