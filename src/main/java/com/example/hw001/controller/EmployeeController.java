package com.example.hw001.controller;

import com.example.hw001.model.Employee;
import com.example.hw001.repository.EmpRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {
    private final EmpRepository empRepository;


    public EmployeeController(EmpRepository empRepository) {
        this.empRepository = empRepository;
    }
    @GetMapping("findbyid")
    public Employee findByID (Long id){
        return empRepository.findById(id);
    }
    @DeleteMapping("deletebyid")
    public void deleteByID (Long id) {
        empRepository.deleteById(id);
    }
    @PostMapping("addemp")
    public Employee addEmp (@RequestBody Employee employee){
        return empRepository.save(employee);
    }
    @PatchMapping("updateemp")
    public Employee update (@RequestBody Employee employee){
        return empRepository.update(employee);
    }
    @GetMapping("getall")
    public List<Employee> getAll (){
        return empRepository.findAll();
    }
}
