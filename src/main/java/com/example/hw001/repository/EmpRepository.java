package com.example.hw001.repository;

import com.example.hw001.model.Employee;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class EmpRepository {
        @PersistenceContext
        private EntityManager entityManager;
        @Transactional
        public Employee save(Employee employee) {
            entityManager.persist(employee);
            return employee;
        }
        public Employee findById(long id) {
            Employee employee = (Employee) entityManager.find(Employee.class, id);
            return employee;
        }
        @Transactional
        public Employee update(Employee employee) {
            entityManager.merge(employee);
            return employee;
        }
        @Transactional
        public Employee deleteById(long id) {
            Employee employee = findById(id);
            if (employee != null) {
                entityManager.remove(employee);
            }
            return employee;
        }
        @Transactional
        public List<Employee> findAll() {
        TypedQuery<Employee> query = entityManager.createQuery("SELECT t FROM Employee t", Employee.class);
        return query.getResultList();
    }
}

